const logger = require("../config/logger");

module.exports = (err, req, res, next) => {
    logger.error({
        service: "default",
        message: err.message,
        error: { ...err },
    });
    // set locals, only providing error in development
    if (!err.status) err.status = 500;
    if (err.status >= 400 && err.status < 500)
        return res.status(err.status).send({ message: err.message });
    res.status(500).send({ message: "Inernal Server Error" });
};
