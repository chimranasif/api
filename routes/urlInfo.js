var express = require("express");
var router = express.Router();
const { urlInfoController } = require("../controllers");

/* GET users listing. */
router.get("/1/*", urlInfoController.getUrlInfo);
router.post("/update", urlInfoController.updateUrlInfo);

module.exports = router;
