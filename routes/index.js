var express = require("express");
const { indexController } = require("../controllers");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
    res.status(200).send();
});

router.get("/ping", indexController.pong);

module.exports = router;
