const logger = require("../../config/logger");
const redisClient = require("../../config/redisConnection");

const setUrl = async (urlId, status) => {
    try {
        console.log("Saving to Redis");
        await redisClient.set(urlId, status);
    } catch (error) {
        logger.debug({ service: redis, message: error.message, error });
        throw error;
    }
};

const setMultipleUrls = async (urls) => {
    try {
        const cacheRecords = [];
        for (const [key, val] of Object.entries(urls)) {
            cacheRecords.push(key);
            cacheRecords.push(val.status);
        }
        await redisClient.mset(cacheRecords);
    } catch (error) {
        logger.debug({ service: redis, message: error.message, error });
        throw error;
    }
};

const getUrl = async (urlId) => {
    try {
        const reply = await redisClient.get(urlId);
        return reply;
    } catch (error) {
        logger.debug({ service: redis, message: error.message, error });
    }
};

module.exports = { setUrl, getUrl, setMultipleUrls };
