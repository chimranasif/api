const firestoreConnection = require("../../config/firestoreConnection");
const logger = require("../../config/logger");

const getUrl = async (urlId) => {
    try {
        const docRef = firestoreConnection.collection("urls").doc(urlId);
        const documentSnapshot = await docRef.get();
        if (documentSnapshot.exists) {
            let url = await documentSnapshot.data();
            logger.debug({
                service: "database",
                message: "returning document from the database.",
                doc: { ...url },
            });
            return url;
        }
        return;
    } catch (error) {
        logger.error({
            service: "database",
            message: error.message,
            error: { ...error },
        });
        throw error;
    }
};

const updateUrls = async (urls) => {
    try {
        const records = Object.entries(urls); //Convert object into array of arrays
        const devider = 500; // Since GCP Firestore only supports 500 writes at a time.

        const chunks = Math.ceil(records.length / devider);

        for (let i = 0; i < chunks; i++) {
            let writeBatch = firestoreConnection.batch();
            const update = records.slice(i * devider, i * devider + devider);

            for (const [key, value] of update) {
                // console.log(key, value);
                let documentRef = firestoreConnection
                    .collection("urls")
                    .doc(key);
                writeBatch.set(documentRef, value);
                // console.log(key, value);
            }
            await writeBatch.commit();
            logger.info({
                service: "database",
                message: `Saved ${update.length} documents.`,
            });
        }
    } catch (error) {
        logger.error({
            service: database,
            message: error.message,
            error: { ...error },
        });
        throw error;
    }
};
module.exports = { getUrl, updateUrls };
