const events = require("events");
const { urlCrud } = require("../database");
const { createUrlsObject } = require("../../utils");
const redis = require("../redis");
const logger = require("../../config/logger");

const urlEvents = new events.EventEmitter();

urlEvents.on("addUknownUrl", async (urls) => {
    try {
        const urlDocs = createUrlsObject(urls);
        await urlCrud.updateUrls(urlDocs);
        await redis.setMultipleUrls(urlDocs);
    } catch (error) {
        logger.error({ service: events, message: error.message, error });
    }
});

urlEvents.on("addToRedis", async (urlId, status) => {
    try {
        await redis.setUrl(urlId, status);
    } catch (error) {
        logger.error({ service: events, message: error.message, error });
    }
});

urlEvents.on("addMutlipleUrlsToRedis", async (urls) => {
    try {
        await redis.setMultipleUrls(urls)
    } catch (error) {
        logger.error({ service: events, message: error.message, error });
    }
});

module.exports = urlEvents;
