FROM node:current-slim
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD ["npm", "start"]


# gcloud builds submit --tag us.gcr.io/na-dev-urlinfo-01/urlinfo-api