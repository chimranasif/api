const winston = require("winston");
const { LoggingWinston } = require("@google-cloud/logging-winston");
const path = require("path");
const config = require(".");

let loggingWinstonConfig;

if (process.env.NODE_ENV === "development")
    loggingWinstonConfig = {
        projectId: "na-urlinfo-sandbox-01",
        keyFilename: path.join(
            __dirname,
            "../keys/na-urlinfo-sandbox-01-23521acb2787.json"
        ),
    };
else
    loggingWinstonConfig = {
        serviceContext: {
            service: "urlInfoApi", // required to report logged errors
            // to the Google Cloud Error Reporting
            // console
            version: "1.0.0",
        },
    };

const loggingWinston = new LoggingWinston(loggingWinstonConfig);

// Create a Winston logger that streams to Stackdriver Logging
// Logs will be written to: "projects/YOUR_PROJECT_ID/logs/winston_log"

const transports = [loggingWinston];
if (
    config.app.environment === "development" ||
    config.app.logLevel === "silly"
) {
    transports.push(new winston.transports.Console());
}
const logger = winston.createLogger({
    level: config.app.logLevel,
    format: winston.format.json(),
    transports: transports,
});

logger.info({ service: "logger", message: "Logger initialized" });

module.exports = logger;
