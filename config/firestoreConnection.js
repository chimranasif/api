const Firestore = require("@google-cloud/firestore");
const path = require("path");

module.exports = new Firestore(
    process.env.NODE_ENV === "development" && {
        projectId: "na-urlinfo-sandbox-01",
        keyFilename: path.join(
            __dirname,
            "../keys/na-urlinfo-sandbox-01-23521acb2787.json"
        ),
    }
);
