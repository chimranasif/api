const config = require(".");
const asyncRedis = require("async-redis");
const logger = require("./logger");

let client;

if (config.redis.enabled) {
    client = asyncRedis.createClient(config.redis.port, config.redis.host);
    client.on("error", (err) => {
        config.redis.connected = false;
        logger.error({
            service: "redis",
            message: err.message,
            stack: err.stack,
        });
    });
    client.on("connect", function () {
        config.redis.connected = true;
        logger.info({
            service: "redis",
            message: "Connected to redis server.",
        });
    });
}

module.exports = client;
