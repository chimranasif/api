const config = {
    app: {
        port: parseInt(process.env.PORT),
        environment: process.env.NODE_ENV,
        logLevel: process.env.LOG_LEVEL,
    },
    redis: {
        host: process.env.REDIS_HOST,
        port: parseInt(process.env.REDIS_PORT),
        enabled: process.env.REDIS_ENABLED === "true" ? true : false,
        connected: false,
    },
    urlInfo: {
        saveUnknown: process.env.SAVE_UNKNOWN === "true" ? true : false,
    },
};

module.exports = config;
