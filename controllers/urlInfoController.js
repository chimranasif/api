// Controller functions for the root endpoint
var createError = require("http-errors");
const crypto = require("crypto");
const { urlCrud } = require("../services/database");
const redis = require("../services/redis");
const { urlEvents } = require("../services/events");
const { createUrlsObject } = require("../utils");
const config = require("../config");
const logger = require("../config/logger");
const {
    updateUrlInfo_validator,
    getUrlInfo_validator,
} = require("./validation");

const getUrlInfo = async (req, res, next) => {
    try {
        const validate = getUrlInfo_validator.validate(
            req.originalUrl.replace(/\/urlinfo\/1\//g, "")
        );
        if (validate.error) throw new createError(400, "Invalid URI");
        const urlParam = validate.value;
        var urlHash = crypto.createHash("md5").update(urlParam).digest("hex");

        logger.debug({
            service: "controllers",
            message: "Request recieved",
            func: "getUrlInfo",
            urlHash,
            url: urlParam,
        });

        if (config.redis.connected) {
            const status = await redis.getUrl(urlHash);
            if (status != null) {
                logger.debug({
                    service: "controller",
                    message: "URL found in redis cache.",
                    func: "getUrlInfo",
                    urlHash,
                    url: urlParam,
                });
                return res.status(200).send({
                    status,
                    url: urlParam,
                });
            }
        }
        const url = await urlCrud.getUrl(urlHash);
        if (url) {
            logger.debug({
                service: "controller",
                message:
                    "URL found in database. Saving it to the redis cache now.",
                func: "getUrlInfo",
                urlHash,
                url: urlParam,
            });
            config.redis.connected &&
                urlEvents.emit("addToRedis", urlHash, url.status);
            return res.status(200).send({
                status: url.status,
                url: urlParam,
            });
        }
        if (config.urlInfo.saveUnknown) {
            logger.debug({
                service: "controller",
                message:
                    "Unable to find the URL. Saving it to the databse and redis cache now.",
                func: "getUrlInfo",
                urlHash,
                url: urlParam,
            });
            urlEvents.emit("addUknownUrl", [{ url: urlParam, status: "NA" }]);
        }
        res.status(200).send({ status: "NA", url: urlParam });
    } catch (error) {
        next(error);
    }
};

const updateUrlInfo = async (req, res, next) => {
    try {
        const { urls } = req.body;
        logger.info({
            service: "controller",
            message: "URL update received.",
            func: "updateUrlInfo",
            totalRecordsReceived: urls.length,
        });
        const validate = updateUrlInfo_validator.validate(urls);
        if (validate.error)
            return res
                .status(400)
                .send({ message: "Invalid URI or Status Provided." });
        console.log(validate);
        const urlDocs = createUrlsObject(validate.value);
        await urlCrud.updateUrls(urlDocs);
        config.redis.connected &&
            urlEvents.emit("addMutlipleUrlsToRedis", urlDocs);
        res.status(200).send({ message: "URLs have been updated." });
    } catch (error) {
        next(error);
    }
};

module.exports = { getUrlInfo, updateUrlInfo };
