let Joi = require('joi')
const url = updateUrlInfo = Joi.object().keys({
  url: Joi.string().uri().required(),
  status: Joi.string().valid('SAFE','UNSAFE', "NA").required(),
})

module.exports = Joi.array().items(url)

