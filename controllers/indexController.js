// Controller functions for the root endpoint

const pong = (req, res, next) => {
    res.status(200).send({ status: "success", message: "pong" });
};

module.exports = { pong };
