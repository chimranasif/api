const request = require("supertest");
const app = require("./app");

describe("GET /urlinfo/1", () => {
    test("Should respond with a 200 status code.", async () => {
        const response = await request(app)
            .get("/urlinfo/1/yahoo.com:8080")
            .send();
        expect(response.statusCode).toBe(200);
    });

    test("Should respond with a 400 status code.", async () => {
        const response = await request(app).get("/urlinfo/1/yahoo").send();
        expect(response.statusCode).toBe(400);
    });
});

describe("POST /urlinfo/update", () => {
    test("Should respond with a 200 status code.", async () => {
        const response = await request(app)
            .post("/urlinfo/update")
            .send({
                urls: [
                    {
                        url: "http://a.example.org/",
                        status: "SAFE",
                    },
                    {
                        url: "http://b.example.org/",
                        status: "SAFE",
                    },
                ],
            });
        expect(response.statusCode).toBe(200);
    });

    test("Should respond with a 400 status code.", async () => {
        const response = await request(app)
            .post("/urlinfo/update")
            .send({
                urls: [
                    {
                        url: "invalidurlexmple",
                        status: "SAFE",
                    },
                    {
                        url: "http://b.example.org/",
                        status: "SAFE",
                    },
                ],
            });
        expect(response.statusCode).toBe(400);
    });

    test("Should respond with a 400 status code.", async () => {
        const response = await request(app)
            .post("/urlinfo/update")
            .send({
                urls: [
                    {
                        url: "http://a.example.org/",
                        status: "INVALIDSTATUS",
                    },
                    {
                        url: "http://b.example.org/",
                        status: "SAFE",
                    },
                ],
            });
        expect(response.statusCode).toBe(400);
    });
});
