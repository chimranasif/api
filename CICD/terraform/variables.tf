variable "project_id" {
  description = "Name of the project"
  type        = string
}

variable "environment" {
  description = "Name of the current deployment environment."
  type        = string
}

variable "api_image" {
  description = "urlinfo api image value from gcr"
  type        = string
}