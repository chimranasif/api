terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.66.1"
    }
  }
}

provider "google" {
  project = var.project_id
  region  = "us-central1"
}
# Storage buckets

# Create a storage bucket for gcr
resource "google_container_registry" "artifact-registry" {
  project = var.project_id
}

# Create a google app engine instance so that a firestore database can be initialized.
# This is a workaround since there is no way to enable firestore database in native mode directly
# Create Cloud Firestore
resource "google_app_engine_application" "app" {
  location_id   = "us-central"
  database_type = "CLOUD_FIRESTORE"
}

# Redis instance
resource "google_redis_instance" "cache" {
  name           = "cache"
  memory_size_gb = 1

  display_name = "Redis Cache"
}

# Create a cloud-run service for the frontend.
resource "google_cloud_run_service" "api" {
  project                    = var.project_id
  name                       = "api"
  location                   = "us-central1"
  autogenerate_revision_name = true
  template {
    spec {
      containers {
        image = var.api_image
        env {
          name  = "LOG_LEVEL"
          value = "info"
        }
        env {
          name  = "NODE_ENV"
          value = var.environment
        }
        env {
          name  = "GCP_PROJECT_ID"
          value = var.project_id
        }

        env {
          name  = "REDISHOST"
          value = google_redis_instance.cache.host
        }
        env {
          name  = "REDISPORT"
          value = google_redis_instance.cache.port
        }
        resources {
          limits = {
            cpu    = "1000m"
            memory = "1024Mi"
          }
        }
      }
      service_account_name = "urlinfo-api-sa@${var.project_id}.iam.gserviceaccount.com"
    }
    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale"        = "0"
        "autoscaling.knative.dev/maxScale"        = "1"
      }
    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth-backend" {
  location    = google_cloud_run_service.api.location
  project     = google_cloud_run_service.api.project
  service     = google_cloud_run_service.api.name
  policy_data = data.google_iam_policy.noauth.policy_data
}
# BACKEND <---