locals {
  url_info_project_id = {
    dev = {
      na = "na-dev-urlinfo-01"
    }
    prod = {
      na = "na-prod-urlinfo-01"
    }
  }
  # starter list for APIs to enable in GCP
  api_services = [
    "appengine.googleapis.com",
    "cloudapis.googleapis.com",
    "cloudbuild.googleapis.com",
    "clouddebugger.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "containerregistry.googleapis.com",
    "deploymentmanager.googleapis.com",
    "eventarc.googleapis.com",
    "firebaserules.googleapis.com",
    "firestore.googleapis.com",
    "logging.googleapis.com",
    "redis.googleapis.com",
    "run.googleapis.com",
    "servicemanagement.googleapis.com",
    "serviceusage.googleapis.com",
    "storage.googleapis.com",
    "storage-api.googleapis.com",
    "storage-component.googleapis.com",

  ]

  project_services = flatten([
    for api in local.api_services : [
      for projects in local.url_info_project_id[var.environment] : {
        object_key = "${projects}_${api}"
        api        = api
        project    = projects
      }
    ]
  ])
}

# Certain services do not come enabled upon initial project creation, enable them for each project

resource "google_project_service" "service_dev" {
  for_each                   = { for services in local.project_services : services.object_key => services }
  project                    = each.value.project
  service                    = each.value.api
  disable_dependent_services = false
  disable_on_destroy         = false
}

resource "google_project_service" "service_prod" {
  for_each                   = { for services in local.project_services : services.object_key => services }
  project                    = each.value.project
  service                    = each.value.api
  disable_dependent_services = false
  disable_on_destroy         = false
}
