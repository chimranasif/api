const Firestore = require("@google-cloud/firestore");
const crypto = require("crypto");
const validUrl = require("valid-url");
const logger = require("../config/logger");

const createUrlsObject = (urls) => {
    try {
        const urlDocs = {};
        urls.forEach((record) => {
            if (validUrl.isUri(record.url)) {
                const urlObject = new URL(record.url);
                const id = crypto
                    .createHash("md5")
                    .update(record.url)
                    .digest("hex");
                const urlDoc = {
                    timestamp: Firestore.FieldValue.serverTimestamp(),
                    protocol: urlObject.protocol,
                    domain: urlObject.host,
                    port: urlObject.port,
                    url: urlObject.href,
                    status: record.status,
                };
                urlDocs[id] = urlDoc;
            }
        });
        return urlDocs;
    } catch (error) {
        logger.error({ message: error.message, error: error });
    }
};

module.exports = createUrlsObject;
