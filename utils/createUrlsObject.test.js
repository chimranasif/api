const createUrlsObject = require("./createUrlsObject");

test("Creates an object from an array with uniq keys.", () => {
    expect(
        createUrlsObject([
            { url: "https://a.com", status: "SAFE" },
            { url: "https://b.com", status: "UNSAFE" },
            { url: "https://b.com", status: "SAFE" },
        ])
    ).toMatchObject({
        "66ccb499bd49d27970d31bd0dca319bd": {},
        dda3424590010381d54574e920b41a3d: {},
    });
});
